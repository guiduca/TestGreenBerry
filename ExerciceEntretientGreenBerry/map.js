'use strict'

angular.module('myApp',[]).
directive('myMap',function(){

	var mapOptions = {
        center: new google.maps.LatLng(50, 2),
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false
    };

	function initMap() {
		if (map === void 0) {
    		map = new google.maps.Map(element[0], mapOptions);
    	}
	}

	initMap();
}