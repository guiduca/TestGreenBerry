'use strict';

var greenBerry = angular.module('greenBerry',[
	'userList','ngMaterial'

]);

var userList = angular.module('userList', []);

userList.controller('userCtrl', ['$scope',
    function ($scope) {

        $scope.users = [];
        $scope.fruitNames = ['Apple', 'Banana', 'Orange'];

        $scope.addUser = function (user) {
            var newUser = $scope.user.name.trim();
            if (!newUser.length) {
                return;
            }
            $scope.users.push({
                title: user.name,
                mail: user.mail,
                completed: false
            });
            $scope.user = '';
        };

        $scope.removeUser = function (index) {
        	$scope.users.splice(index, 1);
        };
    }
]);