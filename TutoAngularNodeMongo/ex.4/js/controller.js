
 var app = angular.module('test', []);


app.controller('monControleur', [ '$scope',function ($scope) {
  // Do something with myService
  		$scope.tabSerie=[
  		{
  			titre:"Le Tône de fer",
  			titreOr: "Game of Thrones",
  			createur:"David Benioff et D. B. Weiss",
  			urlImage:"/image/got.jpg",
  			etat:"en cours saison 06 diffusée au printemps 2016"
  		},
  		{
  			titre:"Lost: Les Disparus",
  			titreOr:"Lost",
  			createur:"J.J.Abrams, Damon Lindelof",
  			urlImage:"/image/lost.jpg",
  			etat:"terminée"
  		},
  		{
  			titre:"Homeland",
  			titreOr:"Homeland",
  			createur:"Howard Gordon et Alex Gansa",
  			urlImage:"/image/Homeland.jpg",
  			etat:"en cours"
  		}
  		]

}]);