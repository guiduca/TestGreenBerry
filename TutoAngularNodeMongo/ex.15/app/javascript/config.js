var app = angular.module('app', ['ngRoute']);

app.config(function($routeProvider){
	$routeProvider
		.when('/home',{templateUrl:'template/ex15_1.html',controller:''})
		.when('/formulaire',{templateUrl:'template/ex15_2.html',controller:''})
		.when('/article',{templateUrl:'template/ex15_3.html',controller:''})
		.otherwise({redirectTo:'/home'});
})