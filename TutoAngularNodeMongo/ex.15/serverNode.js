var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use('/JavaScript', express.static(__dirname + '/app/javascript'));
app.use('/bower_components',express.static(__dirname + '/app/bower_components'));
app.use('/image', express.static(__dirname + '/app/image'));
app.use('/css',express.static(__dirname+'/app/css'));
app.use('/template', express.static(__dirname+'/app/template'));
app.get('/',function (req,res){
	res.sendFile(__dirname+'/app/index.html')
})
app.use(bodyParser());

app.listen(8080);