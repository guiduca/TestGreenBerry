var express = require('express');
var app = express();
app.use('/JavaScript', express.static(__dirname + '/app/javascript'));
app.use('/bower_components',express.static(__dirname + '/app/bower_components'));
app.get('/',function (req,res){
	res.sendFile(__dirname + '/app/index.html');
});

app.listen(8080);