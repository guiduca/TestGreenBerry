var app = angular.module('app', []);

app.controller('monControl', ['$scope', '$http', function($scope, $http){
		$scope.affiche=function(){
			$http.get('/api/affiche')
			.success(function(data){
				console.log(data);
				if (data != 'err'){
					$scope.listePerso=data;
				}
			})
		}

		$scope.traitForm=function(){
		$http.post('/api/formulaire',$scope.perso)
		.success(function(data){
			if (data=='err'){
				alert("Désolé un problème est survenu lors de l'enregistrement");
			}
			else{
				$scope.perso={};
				$scope.affiche();
			}
		})
	}
	$scope.affiche();
}]);