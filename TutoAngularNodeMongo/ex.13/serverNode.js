var ficheInfo=[]

var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use('/JavaScript', express.static(__dirname + '/app/javascript'));
app.use('/bower_components',express.static(__dirname + '/app/bower_components'));
app.use(bodyParser());

app.get('/',function (req,res){
	res.sendFile(__dirname + '/app/index.html');
});

app.get('/api/affiche', function (req, res) {
	res.json(ficheInfo);
});

app.post('/api/formulaire', function(req,res) {
	monPerso=req.body;
	monPerso.id=1+ficheInfo.length
	ficheInfo.push(monPerso);
	res.send();
});

app.listen(8080);