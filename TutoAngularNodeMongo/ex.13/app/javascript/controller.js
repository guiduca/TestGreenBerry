var app = angular.module('test', []);

app.controller('monControl', [ '$scope', '$http',function ($scope,$http){
	$scope.affiche=function(){
		$http.get('/api/affiche')
		.success(function(data){
			console.log(data);
			$scope.listePerso=data;
		})
	}
	$scope.traitForm=function(){
		$http.post('/api/formulaire',$scope.perso)
		.success(function(data){
			$scope.perso={};
			$scope.affiche();
		})
	}
	$scope.affiche();
}]);