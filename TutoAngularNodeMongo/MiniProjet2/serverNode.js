var ficheInfo=[
	{
		id:1,
		nom:"Durand",
		prenom:"Pierre"
	},
	{
		id:2,
		nom:"Dupont",
		prenom:"Christophe"
	},
	{
		id:3,
		nom:"Martin",
		prenom:"Michel"
	},
	{
		id:4,
		nom:"Durand",
		prenom:"Marc"
	},
	{
		id:5,
		nom:"Davoine",
		prenom:"Martin"
	}
]

var ficheTmp = []

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var error = 'Objet inconnu'

app.use('/JavaScript', express.static(__dirname + '/app/javascript'));
app.use('/bower_components',express.static(__dirname + '/app/bower_components'));
app.use(bodyParser());

app.get('/',function (req,res){
	res.sendFile(__dirname + '/app/index.html');
});

app.get('/api/liste', function (req, res) {
	res.json(ficheTmp);
})

app.get('/api/affiche/:id', function (req, res) {
	ficheTmp = [];
		for (var i=0;i<ficheInfo.length;i++){
			if (ficheInfo[i].id==req.params.id){
				//res.json(ficheInfo[i]);
				ficheTmp.push(ficheInfo[i]);
			}
			else if(ficheInfo[i].nom==req.params.id){
				//res.json(ficheInfo[i]);
				ficheTmp.push(ficheInfo[i]);
			}
			else if(ficheInfo[i].prenom==req.params.id){
				//res.json(ficheInfo[i]);
				ficheTmp.push(ficheInfo[i]);
			}
		}
		res.json(ficheTmp);
});

app.listen(8080);