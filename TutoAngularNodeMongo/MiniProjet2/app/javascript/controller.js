var app = angular.module('test', []);

app.controller('monControl', [ '$scope', '$http',function ($scope,$http){
	$scope.afficheFiche=function(){
		$http.get('/api/affiche/'+$scope.ident)
		.success(function(data){
			console.log(data);
			$scope.afficheList();
			$scope.fiche=data;
			$scope.ident="";
		})
	}

	$scope.afficheList=function(){
		$http.get('/api/liste')
		.success(function(data){
			$scope.listeFiche=data;
		})
	}
}]);