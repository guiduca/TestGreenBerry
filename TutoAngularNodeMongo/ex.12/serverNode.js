var ficheInfo=[
	{
		id:1,
		nom:"Depuis",
		prenom:"Jean"
	},
	{
		id:2,
		nom:"Durand",
		Prenom:"Christian"
	},
	{
		id:3,
		nom:"Martin",
		prenom:"Michel"
	}
]

var express = require('express');
var app = express();
app.use('/JavaScript', express.static(__dirname + '/app/javascript'));
app.use('/bower_components',express.static(__dirname + '/app/bower_components'));

app.get('/',function (req,res){
	res.sendFile(__dirname + '/app/index.html');
});
app.get('/api/affiche', function (req, res) {
	res.json(ficheInfo);
});

app.listen(8080);