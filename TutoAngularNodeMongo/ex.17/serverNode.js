var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/maDB17');
var persoSchema=mongoose.Schema({
	nom:{type:String},
	prenom:{type:String},
	age:{type:Number},
	poids:{type:Number}
})
var Perso=mongoose.model('Perso',persoSchema);

var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use('/javascript', express.static(__dirname + '/app/javascript'));
app.use('/bower_components', express.static(__dirname + '/app/bower_components'));
app.use(bodyParser());

app.get('/',function (req, res){
	res.sendfile(__dirname + '/app/index.html');
});
app.post('/api/formulaire', function(req,res){
	var nouveauPerso=new Perso({
		nom:req.body.nom,
		prenom:req.body.prenom,
		age:req.body.age,
		poids:req.body.poids
	});
	nouveauPerso.save(function(err){
		if (err){
			res.send('err');
		}
		else {
			res.send();
		}
	})
});

app.listen(8080)